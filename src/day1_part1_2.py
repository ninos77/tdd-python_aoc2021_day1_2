

# def get_data():
#     data = []
#     with open("data_day1.txt", 'r') as f:
#         for line in f:
#             data.append(int(line.strip()))
#     return data
# data_input = []
# with open("data_day1.txt", 'r') as f:
#         for line in f:
#             data_input.append(int(line.strip()))
data_input = [199,
200,
208,
210,
200,
207,
240,
269,
260,
263]

#========== Part One ================#
def count_number_of_times_a_depth_measurement_increases(data_input):
    incres_count = 0
    for i in range(len(data_input)-1):
        if data_input[i+1] > data_input[i]:
            incres_count +=1
    return incres_count

#=========== Part Two  ================#
def count_number_of_times_the_sum_of_measurements_in_the_window_increases(data_input):
    incres_count = 0
#new_data[i+1]+new_data[i+2]+new_data[i+3] > new_data[i]+new_data[i+1]+new_data[i+2]:
    for i in range(len(data_input)-1):
        if data_input[i+2] == data_input[-1]:
            break
        elif data_input[i+1]+data_input[i+2]+data_input[i+3] > data_input[i]+data_input[i+1]+data_input[i+2]: 
            incres_count +=1
    return incres_count

