import pytest
from src.day2_part1_2 import calculate_horizontal_position_and_depth_after_following_the_course,calculate_final_horizontal_position_multiply_by_final_depth


#data_dow_up,data_forward= get_data()
data = [
'forward 5',
'down 5',
'forward 8',
'up 3',
'down 8',
'forward 2'
]
data_forward=[]
data_dow_up=[]
for l in data:
    if "forward" in l:
        data_forward.append(int(l.strip("forward \n"))) 
    else:
        data_dow_up.append(l.strip())
def test_calculate_horizontal_position_and_depth():
    retVal = calculate_horizontal_position_and_depth_after_following_the_course(data_dow_up,data_forward)
    assert retVal == 150    

def test_calculate_final_horizontal_multiply_final_depth():
    retVal = calculate_final_horizontal_position_multiply_by_final_depth()
    assert retVal == 900

