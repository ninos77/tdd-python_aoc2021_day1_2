import pytest
from src.day1_part1_2 import count_number_of_times_a_depth_measurement_increases,count_number_of_times_the_sum_of_measurements_in_the_window_increases

#data_input = get_data()
data_input = [199,
200,
208,
210,
200,
207,
240,
269,
260,
263]
# with open("data_day1.txt", 'r') as f:
#     for line in f:
#         data_input.append(int(line.strip()))

def test_count_number_of_times_a_depth_increases():
    retVal = count_number_of_times_a_depth_measurement_increases(data_input)
    assert retVal == 7    

def test_count_number_of_times_the_sum_of_measurements_increases():
    retVal = count_number_of_times_the_sum_of_measurements_in_the_window_increases(data_input)
    assert retVal == 5  
