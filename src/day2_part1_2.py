

# def get_data():
#     data_forward=[]
#     data_dow_up=[]
#     with open('data_day2.txt', 'r') as data:
#         for l in data:
#             if "forward" in l:
#                 data_forward.append(int(l.strip("forward \n"))) 
#             else:
#                 data_dow_up.append(l.strip())
     
#     return data_forward,data_dow_up

#data_dow_up,data_forward= get_data()
data = [
'forward 5',
'down 5',
'forward 8',
'up 3',
'down 8',
'forward 2'
]
data_forward=[]
data_dow_up=[]
for l in data:
    if "forward" in l:
        data_forward.append(int(l.strip("forward \n"))) 
    else:
        data_dow_up.append(l.strip())
#========== Part One ================#
def calculate_horizontal_position_and_depth_after_following_the_course(data_dow_up,data_forward):
    total_depth=0
    i_value=0
    for i in data_dow_up:
        if "down" in i:
            i_part= i.split(' ')
            i_value= int(i_part[1])
            total_depth += i_value 
        else:
            i_part= i.split(' ')
            i_value= int(i_part[1])
            total_depth -= i_value     
    return total_depth * sum(data_forward)

#=========== Part Two  ================#

def calculate_final_horizontal_position_multiply_by_final_depth():
    depth = 0
    aim = 0
    position = 0
    data_dic = dict()
    route = []
    for item in data:
        l = item.split(" ")
        data_dic[l[0]] = l[1]
        route.append(data_dic)
        data_dic = dict()
        
    for change in route:
        key = list(change.keys())[0]
        
        value = int(list(change.values())[0])
        
        if key == 'down':
            aim += value
        elif key == 'up':
            aim -= value
        elif key == 'forward':
            position += value
            depth += (value * aim)
    return sum(data_forward) * depth
